import 'package:flutter/material.dart';
import 'package:slidable_file/slidable_file.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo Slidable Files',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepOrangeAccent),
        useMaterial3: true,
      ),
      home: const MyHomePage(title: 'Flutter Demo Slidable Files'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});
  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color.fromARGB(255, 240, 239, 239),
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        title: Text(widget.title),
      ),
      body: ListView(
        children: [
          SlidableFiles(
            // All button actions are defined in the buttonSetting parameter.
            buttonSetting: [
              // A ButtonSetting can have a text, a backgroundColor,
              // an onPressed and/or a textColor default is white.
              ButtonSetting(
                  text: "Delete",
                  backgroundColor: Colors.red[200],
                  textColor: Colors.black,
                  onPressed: () => {}),
            ],
            // The child parameter is what the user sees when
            // the component is not dragged with background color default is white.
            child: const ListTile(
                leading: Text(
                  'Slide me',
                ),
                trailing: Text('1 button'),
              ),
          ),
          SlidableFiles(
            // All button actions are defined in the buttonSetting parameter.
            buttonSetting: [
              // A ButtonSetting can have a text, a backgroundColor,
              // an onPressed and/or a textColor default is white.
              ButtonSetting(
                  text: "Delete",
                  backgroundColor: Colors.red,
                  onPressed: () => {}),
              ButtonSetting(
                  text: "Update",
                  backgroundColor: Colors.blue,
                  onPressed: () => {}),
            ],
            // The child parameter is what the user sees when
            // the component is not dragged with background color default is white.
            child: const ListTile(
                leading: Text('Slide me'),
                trailing: Text('2 button'),
              ),
          ),
          SlidableFiles(
            // All button actions are defined in the buttonSetting parameter.
            buttonSetting: [
              // A ButtonSetting can have a text, a backgroundColor,
              // an onPressed and/or a textColor default is white.
              ButtonSetting(
                  text: "Delete",
                  backgroundColor: Colors.red,
                  onPressed: () => {}),
              ButtonSetting(
                  text: "Update",
                  backgroundColor: Colors.blue,
                  onPressed: () => {}),
              ButtonSetting(
                  text: "Save",
                  backgroundColor: Colors.green,
                  onPressed: () => {}),
            ],
            // The child parameter is what the user sees when
            // the component is not dragged with background color default is white.
            child: const ListTile(
                leading: Text('Slide me'),
                trailing: Text('3 button'),
              ),
          ),
        ],
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
