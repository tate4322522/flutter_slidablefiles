// To parse this JSON data, do
//
//     final buttonSetting = buttonSettingFromJson(jsonString);

import 'package:flutter/material.dart';

class ButtonSetting {
    String text;
    Color? backgroundColor;
    Color? textColor;
    Function()? onPressed;

    ButtonSetting({
        required this.text,
        required this.backgroundColor,
        required this.onPressed,
        this.textColor = Colors.white,
    });
}
