// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:flutter/material.dart';
import 'package:slidable_file/src/util/widget_size.dart';

import 'model/button_setting.dart';

class SlidableFiles extends StatefulWidget {
  const SlidableFiles({
    Key? key,
    required this.buttonSetting,
    required this.child,
  }) : super(key: key);

  //Get all button actions are defined.
  final List<ButtonSetting> buttonSetting;

  //Get child component are defined.
  final Widget child;

  @override
  _SlidableFilesState createState() => _SlidableFilesState();
}

class _SlidableFilesState extends State<SlidableFiles> {
  // This is used to get the size value of the child component.
  Size mySize = Size.zero;

  // This is used to change the position of the child component
  // when the operation changes when dragging.
  double offsetX = 0.0;

  //Check if the child component is finished on drag
  bool isSwiping = false;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: MediaQuery.sizeOf(context).width,
      height: mySize.height,
      child: Directionality(
        textDirection: TextDirection.ltr,
        child: Stack(
          // Setup ButtonSetting component.
          children: [
            for (var i = 0; i < widget.buttonSetting.length; i++)
              Positioned(
                left: i == 1
                    ? MediaQuery.sizeOf(context).width / 5
                    : i == 2
                        ? MediaQuery.sizeOf(context).width / 5 +
                            MediaQuery.sizeOf(context).width / 5
                        : 0.0,
                child: ElevatedButton(
                  onPressed: widget.buttonSetting[i].onPressed,
                  style: ElevatedButton.styleFrom(
                    elevation: 0,
                    shadowColor: Colors.white,
                    fixedSize: Size(
                        MediaQuery.sizeOf(context).width / 5, mySize.height),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(0),
                    ),
                    backgroundColor:
                        widget.buttonSetting[i].backgroundColor, // background
                    foregroundColor:
                        widget.buttonSetting[i].textColor, // foreground
                  ),
                  child: FittedBox(
                    fit: BoxFit.none,
                    child: Text(
                      widget.buttonSetting[i].text,
                      style: const TextStyle(fontWeight: FontWeight.bold),
                    ),
                  ),
                ),
              ),

            // Setup Child component.
            Positioned(
              left: offsetX,
              child: GestureDetector(
                onHorizontalDragUpdate: (details) {
                  setState(() {
                    if (isSwiping) {
                      offsetX += details.delta.dx;
                    } else {
                      isSwiping = true;
                    }
                  });
                },
                onHorizontalDragEnd: (details) {
                  setState(() {
                    // căn chỉnh lúc mở button
                    isSwiping = false;
                    double fullWidth = MediaQuery.sizeOf(context).width;
                    int buttonLength = widget.buttonSetting.length;
                    if (offsetX > fullWidth / 15 && offsetX <= fullWidth / 5) {
                      offsetX = fullWidth / 5;
                    } else if (buttonLength >= 2 &&
                        offsetX > fullWidth / 5 &&
                        offsetX <= (fullWidth / 5) * 2) {
                      offsetX = (fullWidth / 5) * 2;
                    } else if (buttonLength == 3 &&
                        offsetX > (fullWidth / 5) * 2 &&
                        offsetX <= (fullWidth / 5) * 3) {
                      offsetX = (fullWidth / 5) * 3;
                    } else if (offsetX > fullWidth / 10) {
                      offsetX = offsetX > fullWidth / 1.5
                          ? 0.0
                          : buttonLength == 1
                              ? fullWidth / 5
                              : buttonLength == 2
                                  ? (fullWidth / 5) * 2
                                  : (fullWidth / 5) * 3;
                    } else {
                      offsetX = 0.0;
                    }
                  });
                },
                child: Container(
                  width: MediaQuery.sizeOf(context).width,
                  color: Colors.white,
                  // WidgetSize use to get size of Child components.
                  child: WidgetSize(
                    onChange: (Size mapSize) {
                      setState(() {
                        mySize = mapSize;
                      });
                    },
                    child: widget.child,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
